I have created a slight variation of the iOS Weather App. A few differences include:1. Ability to click on day to update main weather temp UI.
2. No UI that shows weather graphics, like sun, cloud, rain etc.
3. City is set to London only. 

The App is best suited to run in Xcode 8 using an iPhone 7 or 6 simulator. Language:The code base is written entirely in Objective-C, though it could have been done using Swift just as easily. 
Libraries:As for any 3rd party libraries, the app felt simple enough that it did not require any. The native iOS libraries were sufficient to produce a smoothly functioning app. 


Testing:
The main way to test the app is to click on the various days of the week and see the temperature update. A weakness in the weather API seems to be that the min and max temperature of the day are not very accurate. It shows a very small difference in the two. The fractional being rounded to an INT then shown as the same temperature. 
Checking with the real iOS weather app there should be at least a 5 to 10 degree difference in max and min temperature. 


Extra Features:
Given more time it would have been nice to add features like a background that changes based on the weather description of sunny, cloudy, rainy etc. 

Another useful feature would be the ability to change cities and countries. 

Also would be good to have day of week come from iOS time etc. Currently the days of the week are static. 