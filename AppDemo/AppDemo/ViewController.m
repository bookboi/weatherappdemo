//
//  ViewController.m
//  AppDemo
//
//  Created by Anan Mallik on 03/11/2016.
//  Copyright © 2016 Anan Mallik. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//UI Variables to be populated with data from JSON:
@property (weak, nonatomic) IBOutlet UILabel *day1_high;
@property (weak, nonatomic) IBOutlet UILabel *day1_low;
@property (weak, nonatomic) IBOutlet UILabel *day2_high;
@property (weak, nonatomic) IBOutlet UILabel *day2_low;
@property (weak, nonatomic) IBOutlet UILabel *day3_high;
@property (weak, nonatomic) IBOutlet UILabel *day3_low;
@property (weak, nonatomic) IBOutlet UILabel *day4_high;
@property (weak, nonatomic) IBOutlet UILabel *day4_low;
@property (weak, nonatomic) IBOutlet UILabel *day5_high;
@property (weak, nonatomic) IBOutlet UILabel *day5_low;

@property (weak, nonatomic) IBOutlet UILabel *mainTemp;

//Button Actions to update the current Day Temp:
- (IBAction)day1Button:(id)sender;
- (IBAction)day2Button:(id)sender;
- (IBAction)day3Button:(id)sender;
- (IBAction)day4Button:(id)sender;
- (IBAction)day5Button:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *day;

@end

@implementation ViewController

NSDictionary *dataDictionary;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadJSONData];
    
    [self populateData];
}


//Here we gather the entire JSON string to be parsed in iOS
-(void) loadJSONData
{
    
    //The URL from the API with default API key attached:
    NSString *url_string = [NSString stringWithFormat: @"http://api.openweathermap.org/data/2.5/forecast?id=2643743&appid=b3e562c7a3ecec266ccbf90fc9e3ffb0&units=metric"];
    
    NSURL *mainURL = [NSURL URLWithString:url_string];
    NSData *jsonData = [NSData dataWithContentsOfURL:mainURL];
    NSError *error = nil;
    
    //Store the JSON data in NSDictionary:
    dataDictionary = [NSJSONSerialization
                      JSONObjectWithData:jsonData options:0 error:&error];

}

//Here we assign the JSON data to the appropriate UI elements.
//It will start with the data for Day 1:
-(void)populateData
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][0][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    
    
    //Day1:
    temp = dataDictionary[@"list"][0][@"main"][@"temp_max"];
    temperature = [format stringFromNumber:temp];
    self.day1_high.text = temperature;
    self.day1_high.text = [self.day1_high.text stringByAppendingString:@"°"];
    
    temp = dataDictionary[@"list"][0][@"main"][@"temp_min"];
    temperature = [format stringFromNumber:temp];
    self.day1_low.text = temperature;
    self.day1_low.text = [self.day1_low.text stringByAppendingString:@"°"];
    
    
    
    //Day2:
    temp = dataDictionary[@"list"][1][@"main"][@"temp_max"];
    temperature = [format stringFromNumber:temp];
    self.day2_high.text = temperature;
    self.day2_high.text = [self.day2_high.text stringByAppendingString:@"°"];
    
    temp = dataDictionary[@"list"][1][@"main"][@"temp_min"];
    temperature = [format stringFromNumber:temp];
    self.day2_low.text = temperature;
    self.day2_low.text = [self.day2_low.text stringByAppendingString:@"°"];
    
    
    
    //Day3:
    temp = dataDictionary[@"list"][2][@"main"][@"temp_max"];
    temperature = [format stringFromNumber:temp];
    self.day3_high.text = temperature;
    self.day3_high.text = [self.day3_high.text stringByAppendingString:@"°"];
    
    temp = dataDictionary[@"list"][2][@"main"][@"temp_min"];
    temperature = [format stringFromNumber:temp];
    self.day3_low.text = temperature;
    self.day3_low.text = [self.day3_low.text stringByAppendingString:@"°"];
    
    
    
    //Day4:
    temp = dataDictionary[@"list"][3][@"main"][@"temp_max"];
    temperature = [format stringFromNumber:temp];
    self.day4_high.text = temperature;
    self.day4_high.text = [self.day4_high.text stringByAppendingString:@"°"];
    
    temp = dataDictionary[@"list"][3][@"main"][@"temp_min"];
    temperature = [format stringFromNumber:temp];
    self.day4_low.text = temperature;
    self.day4_low.text = [self.day4_low.text stringByAppendingString:@"°"];
    
    
    
    //Day5:
    temp = dataDictionary[@"list"][4][@"main"][@"temp_max"];
    temperature = [format stringFromNumber:temp];
    self.day5_high.text = temperature;
    self.day5_high.text = [self.day5_high.text stringByAppendingString:@"°"];
    
    temp = dataDictionary[@"list"][4][@"main"][@"temp_min"];
    temperature = [format stringFromNumber:temp];
    self.day5_low.text = temperature;
    self.day5_low.text = [self.day5_low.text stringByAppendingString:@"°"];
    
}

//Update data to Day 1:
- (IBAction)day1Button:(id)sender
{
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][0][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    self.day.text = @"Friday";
    
}

//Update data to Day 2:
- (IBAction)day2Button:(id)sender
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][1][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    self.day.text = @"Saturday";
    
}

//Update data to Day 3:
- (IBAction)day3Button:(id)sender
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][2][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    self.day.text = @"Sunday";
    
}

//Update data to Day 4:
- (IBAction)day4Button:(id)sender
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][3][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    self.day.text = @"Monday";
}

//Update data to Day 5:
- (IBAction)day5Button:(id)sender
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setGeneratesDecimalNumbers:NO];
    [format setMaximumFractionDigits:0];
    
    //Truncate the decimal to a whole number:
    NSNumber *temp = dataDictionary[@"list"][4][@"main"][@"temp"];
    NSString *temperature = [format stringFromNumber:temp];
    self.mainTemp.text = temperature;
    self.mainTemp.text = [self.mainTemp.text stringByAppendingString:@"°"];
    
    self.day.text = @"Tuesday";
    
}

- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];

}


@end
